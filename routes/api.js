const express = require("express");
const router = express.Router();
const axios = require("axios");

// Config Defaults Axios dengan Detail Akun Rajaongkir
axios.defaults.baseURL = "https://pro.rajaongkir.com/api";
axios.defaults.headers.common["key"] = "99fdfaf434aba29e835c28c135aa2c9c"; //"47b03835045cf1838b896a7bd507dc2d";
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";

// Router GET province
router.get("/provinsi", (req, res) => {
  axios
    .get("/province")
    .then((response) => res.json(response.data))
    .catch((err) => {
      res.send(err);
    });
});

// Router GET city by province_id
router.get("/kota/:provId", (req, res) => {
  const id = req.params.provId;
  axios
    .get(`/city?province=${id}`)
    .then((response) => res.json(response.data))
    .catch((err) => res.send(err));
});

// Router GET subdistrict by city_id
router.get("/kecamatan/:cityId", (req, res) => {
  const id = req.params.cityId;
  axios
    .get(`/subdistrict?city=${id}`)
    .then((response) => res.json(response.data))
    .catch((err) => res.send(err));
});

// Router POST costs
router.post("/ongkir", (req, res) => {
  const body = req.body;
  axios
    .post("/cost", {
      origin: body.origin,
      originType: "city",
      destination: body.destination,
      destinationType: "city",
      weight: body.weight,
      courier: body.courier,
    })
    .then((response) => {
      console.log(response);
      res.json(response.data);
    })
    .catch((err) => {
      console.log(err);
      res.send(err);
    });
});

module.exports = router;
